import { NestFactory } from '@nestjs/core';
import { MicroserviceOptions, Transport } from '@nestjs/microservices';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { ProfileModule } from './profile.module';
import { ConfigService } from '@nestjs/config';
import { Logger, ValidationPipe } from '@nestjs/common';

import 'dotenv/config';

var cors = require('cors')

async function bootstrap() {
  const app = await NestFactory.create(ProfileModule);

  const config = new DocumentBuilder()
    .setTitle('Kwetter profile service')
    .setDescription('This is the kwetter profile service for Joost.')
    .setVersion('1.0')
    .addTag('profile')
    .build();
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('api', app, document);

  app.use(cors())

  const configService = app.get<ConfigService>(ConfigService);

  app.connectMicroservice<MicroserviceOptions>({
    transport: Transport.KAFKA,
    options: {
      client: {
        clientId: 'profile-service',
        brokers: configService.get<string>('BROKERS').split(','),
      },
    },
  });

  app.useGlobalPipes(new ValidationPipe());

  await app.listen(process.env.PORT, "0.0.0.0");

  const logger = new Logger('Profile Service Nest Application');
  logger.log(`Profile service is running on: ${await app.getUrl()}`);
}
bootstrap();
