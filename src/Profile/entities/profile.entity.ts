import * as typeorm from 'typeorm';

@typeorm.Entity()
export default class Profile {
    constructor() {
        this.profileId = '';
        this.name = '';
        this.description = '';
        this.imageUrl = '';
        this.occupation = '';
        this.facebookLink = '-';
        this.twitterLink = '-';
        this.githubLink = '-';
        this.instagramLink = '-';
        this.youtubeLink = '-';
        this.ownerId = '';
        this.profileCreated = new Date();
        this.profileUpdated = new Date();
        this.lastLoggedIn = new Date();
    }

    @typeorm.PrimaryGeneratedColumn('uuid')
    profileId?: string;

    @typeorm.Column({
        unique: true,
    })
    name!: string;

    @typeorm.Column()
    description!: string;

    @typeorm.Column()
    imageUrl!: string;

    @typeorm.Column()
    occupation!: string;

    @typeorm.Column()
    facebookLink!: string;

    @typeorm.Column()
    twitterLink!: string;

    @typeorm.Column()
    githubLink!: string;

    @typeorm.Column()
    instagramLink!: string;

    @typeorm.Column()
    youtubeLink!: string;

    @typeorm.Column({
        unique: true,
    })
    ownerId!: string;

    @typeorm.CreateDateColumn()
    profileCreated?: Date;
  
    @typeorm.UpdateDateColumn()
    profileUpdated?: Date;

    @typeorm.UpdateDateColumn()
    lastLoggedIn?: Date;
}