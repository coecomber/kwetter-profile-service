import { ApiProperty } from '@nestjs/swagger';
import { IsArray, IsEnum, IsNumber, IsString } from 'class-validator';

export class CreateProfile {
  @IsString()
  @ApiProperty({ type: String })
  name: string;

  @IsString()
  @ApiProperty({ type: String })
  ownerId: string;

  @IsString()
  @ApiProperty({ type: String })
  description: string;

  @IsString()
  @ApiProperty({ type: String })
  imageUrl: string;

  @IsString()
  @ApiProperty({ type: String })
  occupation: string;

  @IsString()
  @ApiProperty({ type: String })
  facebookLink: string;

  @IsString()
  @ApiProperty({ type: String })
  twitterLink: string;

  @IsString()
  @ApiProperty({ type: String })
  githubLink: string;

  @IsString()
  @ApiProperty({ type: String })
  instagramLink: string;

  @IsString()
  @ApiProperty({ type: String })
  youtubeLink: string;
}