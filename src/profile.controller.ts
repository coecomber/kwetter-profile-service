import { Body, Controller, Get, Logger, Param, Post, Req } from '@nestjs/common';
import { ProfileService } from './profile.service';
import Profile from './Profile/entities/profile.entity';
import { CreateProfile } from './Profile/dto/profile.dto';
import { get } from 'http';

@Controller('profile')
export class ProfileController {
  constructor(
    private readonly profileService: ProfileService
  ) { }

  private readonly logger = new Logger(ProfileController.name);
  
  @Get('/getOne/:name')
  async getOneProfileByName(@Param('name') name): Promise<Profile> {
    this.logger.log('Get (one) called with name:' + name)
    return await this.profileService.getOneProfileByName(name);
  }

  @Get('/getOneById/:ownerId')
  async getOneProfileByOwnerId(@Param('ownerId') ownerId): Promise<Profile> {
    this.logger.log('Get (one) called with id:' + ownerId)
    return await this.profileService.getOneProfileById(ownerId);
  }

  @Get('/check/:ownerId')
  async checkIfProfileExistsByOwnerId(@Param('ownerId') ownerId): Promise<Profile> {
    this.logger.log('Get (check) called with id:' + ownerId)
    return await this.profileService.checkIfProfileExistsByOwnerId(ownerId);
  }

  @Get('/profiles/:pageNumber')
  async getAllProfilesBasedOnPageNumber(@Param('pageNumber') pageNumber: number){
    this.logger.log('Get pagenummer called with number:' + pageNumber)
    return this.profileService.getAllProfilesBasedOnPageNumber(pageNumber);
  }
  
  @Get('/getAllProfiles')
  async getAllProfiles(): Promise<Profile[]>{
    this.logger.log('Get all profiles was called.')
    return this.profileService.getAllProfiles();
  }

  @Post()
  async createProfile(
    @Body() profileDto: CreateProfile,
  ): Promise<Profile> {
    this.logger.log('New kweet created' + profileDto)
    const profile: Profile = { ...profileDto }
    console.log(profile)
    return await this.profileService.createProfile(profile);
  }

}
