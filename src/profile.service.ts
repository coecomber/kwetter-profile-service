import { Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import Profile from './Profile/entities/profile.entity';

@Injectable()
export class ProfileService {
  private readonly logger = new Logger(ProfileService.name);

  constructor(
    @InjectRepository(Profile)
    private profileRepoistory: Repository<Profile>,
  ) { }

  async getAllProfiles(): Promise<Profile[]> {
    return this.profileRepoistory.find();
  }

  async getOneProfileByName(name: string): Promise<Profile> {
    return await this.profileRepoistory.findOne({
      where:
        { name: name }
    }
    );
  }

  async getOneProfileById(id: string): Promise<Profile> {
    return await this.profileRepoistory.findOne({
      where:
        { ownerId: id }
    }
    );
  }

  async checkIfProfileExistsByOwnerId(ownerId: string): Promise<Profile> {
    return await this.profileRepoistory.findOne({
      where:
        { ownerId: ownerId }
    });
  }

  async createProfile(profile: Profile): Promise<Profile> {
    return await this.profileRepoistory.save(profile);
  }

  async getAllProfilesBasedOnPageNumber(pageNumber: number) {
    const take = 10;
    const skip = pageNumber * 10;

    const [result, total] = await this.profileRepoistory.findAndCount(
      {
        take: take,
        skip: skip
      }
    );

    return { data: result, count: total };
  }
}
