import { Module } from '@nestjs/common';
import { ProfileController } from './profile.controller';
import { ProfileService } from './profile.service';
import { ConfigModule } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import Profile from './Profile/entities/profile.entity';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
    }),
    TypeOrmModule.forFeature([
      Profile
    ]),
    TypeOrmModule.forRoot(),
  ],
  controllers: [ProfileController],
  providers: [ProfileService],
})
export class ProfileModule {}
